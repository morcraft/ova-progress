const _ = require('lodash')
const checkTypes = require('check-types')
const pendingActivities = require('./pendingActivities.js')
module.exports = {
    handleState: function(args){
        checkTypes.assert.object(args)
        checkTypes.assert.object(args.state)
        if(!checkTypes.object(state.complete)){
            args.state.complete = _args;
        }
        else{
            _.merge(args.state.complete, _args);
        }
        return args.state;
    },
    methods: {
        addCompleteActivities: function(args){
            this.handleState(args);
            checkTypes.assert.object(args);
            checkTypes.assert.object(args.elements);
            _.merge(args.state.complete, args.elements);
            pendingActivities.deletePendingActivities(args);
        },
        deleteCompleteActivities: function(args){
            this.handleState(args);
            checkTypes.assert.object(args);
            checkTypes.assert.object(args.elements);
            _.forEach(args.elements, function(v, k){
                delete this.state.complete[v];
            })
            pendingActivities.addPendingActivities(args);
        }
    }
}