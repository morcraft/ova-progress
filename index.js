/**
 * /**
 * @file Archivo que contiene el módulo ova-progress
 * @namespace index
 * @module ova-progress
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 * @requires module:check-types
 * @requires module:ova-api
 */

const $ = require('jquery')
const _ = require('lodash')
const ovaAPI = require('ova-api')
const checkTypes = require('check-types')
const Signal = require('signals')
/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = function(args){
    if(!checkTypes.object(args)){
        args = {}
    }

    var self = this

    self.APIInstance = new ovaAPI()
    self.updated = new Signal()

    if(!_.isObject(args.courseData)){
        throw new Error('A courseData object property is expected.')
    }

    checkTypes.assert.string(args.courseData.shortName)
    checkTypes.assert.number(args.courseData.unit)
    self.courseData = args.courseData

    if(!checkTypes.object(args.topics)){
        if(args.showFeedback !== false){
            console.warn('Some topics were expected. Initializing with no topics.')
        }
        args.topics = {}
    }
    
    self.topics = {
        all: args.topics,
        finished: {},
        pending: function(){
            return _.omit(self.topics.all, _.keys(self.topics.finished))
        }
    }

    self.handleTopics = function(_args){
        checkTypes.assert.object(_args)
        checkTypes.assert.object(_args.elements)
        if(!checkTypes.object(self.topics.all)){
            self.topics.all = {}
        }
    }

    self.handleFinishedTopics = function(_args){
        checkTypes.assert.object(_args)
        checkTypes.assert.object(_args.elements)
        if(!checkTypes.object(self.topics.finished)){
            self.topics.finished = {}
        }
    }

    self.performPostOperations = function(_args){
        if(checkTypes.object(_args)){
            _args = {}
        }
        if(_args.setProgressBarAmount !== false){
            self.setProgressBarAmount()
        }

        if(_args.updateVisualClue !== false){
            self.updateVisualClue()
        }
    }

    self.deleteFinishedTopics = function(_args){
        self.handleFinishedTopics(_args)
        self.topics.finished = _.omit(self.topics.finished, _.keys(_args.elements))
        self.performPostOperations(_args)        
    }

    self.addFinishedTopics = function(_args){
        self.handleFinishedTopics(_args)
        _.merge(self.topics.finished, _args.elements)
        self.performPostOperations(_args)
    }

    self.updateFinishedTopics = function(_args){
        checkTypes.assert.object(_args)
        checkTypes.assert.object(_args.topics)
        if(_args.action === 'add'){
            _args.method = 'addLOFinishedTopics'
        }
        else{
            if(_args.action === 'delete'){
                _args.method = 'deleteLOFinishedTopics'
            }
            else{
                throw new Error('Unknown action ' + _args.action)
            }
        }
        return self.APIInstance.makeRequest({
            component: 'finishedTopics',
            method: _args.method,
            arguments: {
                shortName: self.courseData.shortName,
                unit: self.courseData.unit,
                topics: _.keys(_args.topics)
            }
        })
        .then(function(response){
            if(response !== true){
                swal({
                    type: 'error',
                    title: 'Error en la respuesta del servidor.',
                    text: 'Ocurrió un error al sincronizar tu progreso'
                })
                if(_args.showFeedback !== false){
                    console.error(response)
                }
                throw new Error("Expected a true response.")
            }
            
            else{
                if(_args.action === 'add'){
                    self.addFinishedTopics({
                        elements: _args.topics
                    })
                }
                else{
                    self.deleteFinishedTopics({
                        elements: _args.topics
                    })
                }
            }

            self.updated.dispatch(self.topics)
            return response
        })
    }

    self.getFinishedTopics = function(_args){
        checkTypes.assert.object(_args)
        return self.APIInstance.makeRequest({
            component: 'finishedTopics',
            method: 'getLOFinishedTopics',
            arguments: {
                shortName: self.courseData.shortName,
                unit: self.courseData.unit,
            }
        })
    }
    
    self.renderProgressBar = function(_args){
        checkTypes.assert.object(_args)
        if(!_args.$target){
            throw new Error("A target is expected.")
        }
        if(!_args.$target instanceof jQuery){
            throw new Error("Invalid target. A jQuery element was expected.")
        }
        if(!_args.$target.length){
            throw new Error("jQuery target not available in DOM.")
        }

        if(self.$progressBar instanceof jQuery && self.$progressBar.length){
            throw new Error("There already exists a progress bar for this instance.")
        }

        return self.$progressBar = $('<div class="progress-bar">' + 
                    '<div class="inner-bar"></div>' +
                '</div>')
                .appendTo(_args.$target)
    }

    self.updateVisualClue = function(){
        var finished = {}
        _.forEach(self.topics.finished, function(v, k){
            if(_.isObject(self.topics.all[k])) finished[k] = v
        })
        
        var title = 'Temas finalizados: ' + _.size(finished) + ' / ' + _.size(self.topics.all)

        return self.$progressBar.attr('title', title)
    }

    self.setProgressBarAmount = function(_args){
        if(checkTypes.not.object(_args)){
            _args = {}
        }
        if(checkTypes.not.number(_args.amount)){
            var filtered =  _.pick(self.topics.all, _.keys(self.topics.finished))
            _args.amount = _.size(filtered) * 100 / _.size(self.topics.all)
        }
        else{
            if(_args.amount < 0 || _args.amount > 100){
                throw new Error("Invalid amount to set progress bar to.")
            }
        }

        self.updateVisualClue()

        return self.$progressBar.children('.inner-bar').css({
            'width': _args.amount + '%'
        })
        
    }
}